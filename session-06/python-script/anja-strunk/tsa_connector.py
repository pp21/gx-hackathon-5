import json
import requests

def post_claims(claims, url: str, cache_key):
    print("sending claims to TSA...")

    # build header
    headers = dict()
    headers['x-cache-key'] = cache_key
    headers['x-cache-namespace'] = cache_key
    headers['x-cache-scope'] = cache_key

    # trigger request
    response = requests.post(url, json = claims, headers = headers)

    # parse response
    status_code = response.status_code
    if status_code == 201:
        print("sending claims to TSA...finished successfully. (HTTP Status code: " + str(status_code) + ").")
    else:
        raise Exception("Something went wrong. HTTP Status code: " + str(status_code))


def get_self_description(url):
    print("calling TSA for Self Description...")
    response = requests.get(url)
    status_code = response.status_code
    if status_code == 200:
        json_response = response.json()
        if "result" in json_response.keys() and json_response['result'] == 'export request is accepted':
            # request TSA a second time
            return get_self_description(url)
        if 'verifiableCredential' in json_response.keys():
            print("calling TSA for Self Description...finished successfully (HTTP Status code: " + str(status_code) + ").")
            return json_response
    else:
        raise Exception("Calling TSA for self-description ... failed. No self-description found at url '" + url +
                        "'\n HTTP Status code: " + str(status_code) +
                        "\nResponse: \n" + json.dumps(response.json(), indent=4, sort_keys=True))

#if __name__ == '__main__':
#   claims = load_json_file(PATH_TO_CLAIMS)
#   post_claims(claims, TSA_POST_URL, "provider")
#   sd = get_self_description(TSA_GET_URL)
#   write_json_file(sd, os.path.abspath(PAHT_TO_FILE))
