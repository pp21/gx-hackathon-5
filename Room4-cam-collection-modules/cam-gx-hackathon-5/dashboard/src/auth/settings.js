
import { Log, OidcClient } from 'oidc-client-ts';
import * as data from '../data/data'

Log.setLogger(console);
Log.setLevel(Log.INFO);


// Set here if the app is running on localhost; true: localhost
if (localStorage.getItem('runLocal') == null) localStorage.setItem('runLocal', 'false')
if (localStorage.getItem('testdata') == null) localStorage.setItem('testdata', 'false')

if (localStorage.getItem('localSiteURL') == null) localStorage.setItem('localSiteURL', 'http://localhost:3000')
if (localStorage.getItem('localAPIURL') == null) localStorage.setItem('localAPIURL', 'http://localhost:8080')
if (localStorage.getItem('prodURL') == null) localStorage.setItem('prodURL', 'https://cam.gxfs.dev')

export const settings = {
    authority: " https://as.aisec.fraunhofer.de/auth",
    client_id: "gaiax-fs",
    redirect_uri: data.getSiteURL() + "/#/loggedin",
    post_logout_redirect_uri: data.getSiteURL() + "/#/loggedout",
    response_type: "code",
    scope: "openid",
    //monitorSession: false,
    //response_mode: "fragment",
    //filterProtocolClaims: true
};

export {
    Log,
    OidcClient
};