#!/usr/bin/env python3
from tls import *
import heartbleed
from camlog import *
import logging
setuplogger(logging.INFO)


hosts = """
gaia-x.eu
www.aisec.fraunhofer.de
""".split()

for host in hosts:
    tmp = host.split(':')
    host = tmp[0]
    if len(tmp) > 1:
        port = int(tmp[1])
    else:
        port = 443
    raw, ret = measure_certtrust(host, port)
    raw, ret = measure_version(host, port)
    raw, ret = measure_ciphersuites(host, port)
    ret = heartbleed.measure_heartbleed(host, port)
    info(f"Heart: {ret}")
