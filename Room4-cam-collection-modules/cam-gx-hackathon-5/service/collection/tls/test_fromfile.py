#!/usr/bin/env python3
from main import *
from test_main import *
import fileinput

if __name__ == '__main__':
    setuplogger(logging.INFO)
    collector = CollectorTls()
    evaluationmock = EvaluationMock()

    def line2req(x): return StartCollectingRequest(
        service_id=f"{x.strip()}:443", eval_manager='localhost:50100')
    collector.client.StartCollectingStream(
        map(line2req, fileinput.FileInput()))

    # TODO: wait on all jobs completed instead of sleeping arbitrary number of seconds
    sleep(100)
    collector.stop(grace=2)
    evaluationmock.stop(grace=2)
